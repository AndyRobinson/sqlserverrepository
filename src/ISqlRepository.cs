﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace SQLRepository
{
    public interface ISqlRepository
    {
        string ConnectionString { get; set; }

        int CommandTimeout { get; set; }

        IDataProvider DataProvider { get; set; }

        int? TotalRecords { get; set; }

        DataSet Execute();

        IEnumerable<T> Execute<T>(Func<IDataReader, T> func);

        IEnumerable<T> Execute<T>(IBuilder<T> builder);

        IEnumerable<T> Execute<T>();

        int ExecuteNonQuery();

        T ExecuteScalar<T>();

        ISqlRepository WithStoredProcedure(string storedProcName);

        ISqlRepository WithSqlStatement(string sqlStatement);

        ISqlRepository AddParameter(string name, object value);

        ISqlRepository AddParameter(SqlParameter sqlParameter);

        void BulkCopy(string destinationTable, DataTable table);

        void BulkCopy(string destinationTable, IDataReader dataReader);
    }
}