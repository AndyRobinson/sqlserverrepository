using System.Collections.Generic;

namespace SQLRepository
{
    public interface IBuilder<T>
    {
        List<T> Build(IDataProvider dataProvider);
    }
}