using System.Collections.Generic;
using System.Linq;
using Dapper;

namespace SQLRepository
{
    public class DataProvider : IDataProvider
    {
        public IEnumerable<T> Read<T>()
        {
            return this.Results.Read<T>().ToList();
        }

        public IEnumerable<dynamic> Read()
        {
            return this.Read<dynamic>().ToList();
            //return Results.Read().ToList();
        }

        public SqlMapper.GridReader Results { get; set; }
    }
}