using System.Collections.Generic;
using Dapper;

namespace SQLRepository
{
    public interface IDataProvider
    {
        IEnumerable<T> Read<T>();

        IEnumerable<dynamic> Read();

        SqlMapper.GridReader Results { get; set; }
    }
}